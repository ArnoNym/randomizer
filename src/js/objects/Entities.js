import {Entity} from "@/js/classes/Entity";

export default {
    values: [],

    newEntity() {
        this.values.push(new Entity(""));
    },
    removeEntity(entity) {
        const index = this.values.indexOf(entity);
        if (index > -1) {
            this.values.splice(index, 1);
        }
    },

    // Methods that you need, for e.g fetching data from server etc.

    fetchData() {
        // fetch logic
    }
}