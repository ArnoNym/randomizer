let id = 0;

export class Entity {
    constructor(name) {
        this.name = name;
        this.id = id;
        id++;
    }
    id;
    name;
}